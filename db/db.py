import itertools
from clickhouse_driver import connect, Client

COUNTRIES_TABLE_NAME = "countries"
DATA = [
    [1, "Afghanistan", "AFG"],
    [2, "Bangladesh", "BGD"],
    [3, "Cabo Verde", "CPV"],
    [4, "Denmark", "DNK"],
    [5, "Ecuador", "ECU"],
    [6, "Fiji", "FJI"],
]

def is_db_initialized() -> bool:
    """The database is considered initialized if
    the table exists and it has a non-zero row count
    """
    client = Client(host='localhost')
    tables = client.execute('SHOW TABLES')
    tables = list(itertools.chain(*tables)) # flatten list
    if tables.count(COUNTRIES_TABLE_NAME) == 0:
        return False
    
    row_count_list = client.execute(f'SELECT COUNT(*) FROM {COUNTRIES_TABLE_NAME}')
    row_count = row_count_list[0][0] # access first element
    return row_count > 0


def initialize_db():
    """create table and insert rows from DATA"""
    print("Initialize database")
    client = Client(host='localhost')
    client.execute(f'DROP TABLE IF EXISTS {COUNTRIES_TABLE_NAME}')
    client.execute(f'CREATE TABLE {COUNTRIES_TABLE_NAME} (id Int32, country_name varchar(255), iso3 varchar(3)) ENGINE = Memory')
    client.execute(f'INSERT INTO {COUNTRIES_TABLE_NAME} (id, country_name, iso3) VALUES', DATA)


def read_all_rows(sort_order):
    client = Client(host='localhost')
    rows = client.execute(f'SELECT * FROM {COUNTRIES_TABLE_NAME} ORDER BY id {sort_order}')
    return rows


def drop_database_table(ctx, param, value):
    """Deletes table: COUNTRIES_TABLE_NAME"""
    if not value or ctx.resilient_parsing:
        return
    print(f"Drop database table: {COUNTRIES_TABLE_NAME}")
    client = Client(host='localhost')
    client.execute(f'DROP TABLE IF EXISTS {COUNTRIES_TABLE_NAME}')
    ctx.exit()
