# Readme

## Setup virtualenv

```bash
pip3 install virtual
python3 -m venv my_venv
source my_venv/bin/activate
pip3 install --upgrade pip
pip3 install --upgrade setuptools
pip3 install -r requirements.txt
```

## Run

How to run the program:

```bash
python main.py
```

Other options:

```bash
python main.py --sort-order=asc
python main.py --sort-order=desc
python main.py --rm
python main.py --help
```

## ClickHouse database

Run in Docker:

```bash
docker run -p 8123:8123 -p 9000:9000 --name my-clickhouse-server --ulimit nofile=262144:262144 yandex/clickhouse-server
```

The Docker image does not work on Apple M1 silicon.
Use an OSX prebuild instead with custom settings (clickhouse/config.xml):

```bash
cd clickhouse
./clickhouse server
```

[Config](https://github.com/ClickHouse/ClickHouse/blob/master/programs/server/config.xml)

## Dependencies

### Click

CLI arg parsing.

Link: https://github.com/pallets/click

### Pandas

Does not install with `brew` on Apple M1 silicon.
Some users report better success with `conda`.

### Dataprep

Util for cleaning and standardizing data.

Here it's used to convert iso3 to iso2 country codes.

## Links

- [Country codes](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes)
- [Clickhouse driver quickstart](https://clickhouse-driver.readthedocs.io/en/latest/quickstart.html#)
