import click
import pandas as pd
from dataprep.clean import clean_country
from prettytable import PrettyTable
from db.db import is_db_initialized, initialize_db, read_all_rows, drop_database_table

@click.command()
@click.option("-so", "--sort-order",
    prompt="Sort order",
    help="How to sort the output. Valid options are \"asc\" or \"desc\" for accending and decending order respectively",
    type=click.Choice(['desc', 'asc'], case_sensitive=False),
    default="asc",
    show_default=True,
    )
@click.option("--rm", is_flag=True, help="Wipe the database and exit.", is_eager=True, expose_value=False, callback=drop_database_table)

def main(sort_order):
    """Reads ISO3 country codes from a ClickHouse database.
    Convert them to ISO2 format using a Pandas DataFrame.
    And finally prints them to stdout with Prettytable.
    """
    click.echo(f"You have selected: {sort_order}")
    is_init = is_db_initialized()
    if not is_init:
        initialize_db()
    rows = read_all_rows(sort_order)
    df = pd.DataFrame(rows, columns=["ID", "Country", "ISO3"])
    # df["ISO2"] = df.apply(lambda row: toIso2(row), axis=1)

    df = clean_country(df, 'ISO3', input_format="alpha-3", output_format='alpha-2', inplace=True)
    df.rename(columns={'ISO3_clean': 'ISO2'}, inplace=True)

    x = PrettyTable()
    x.field_names = df.columns.tolist()
    x.add_rows(df.values.tolist())
    print(x)


# def toIso2(row):
#     if row["ISO3"] == "DNK":
#         return "DK"
#     if row["ISO3"] == "AFG":
#         return "AF"
#     return "??"
    

if __name__ == '__main__':
    main()
